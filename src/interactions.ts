import Vue from 'vue'
import * as d3 from "d3"
import { DragState, MoveType } from "./structures"


export function initDrag(self) {
    let xlabels = d3.selectAll('.colm')
    let ylabels = d3.selectAll('.line')
    let svg = document.querySelector('svg')
    let svgd3 = d3.select('svg')

    let automata = {
      state: DragState.Idle,
      pos: {x: 0, y: 0},
      elts: null,
      symelts: null,
      move: MoveType.Line,
      index: 0
    }

    let _mouseEnter = (d,i) => {
      document.body.style.cursor = 'move'
    }

    let _mouseOut = (d,i) => {
      document.body.style.cursor = 'auto'
    }

    // add mousedown and mousehover handler for lines
    xlabels.each( (e,i) => {
      let elt = d3.select('.colm.colm-'+(i+1))
      let _mouseDown = (d,i2) => {
        let pos = d3.mouse(svg) // returns [x,y]
        automata = {
          state: DragState.Moving,
          pos: { x: pos[0], y: pos[1]},
          elts: d3.selectAll('.colm-'+(i+1)),
          symelts: d3.selectAll('.line-'+(i+1)),
          move: MoveType.Column,
          index: i
        }
      }
      elt.on('mousedown', _mouseDown)
      elt.on('mouseenter', _mouseEnter)
      elt.on('mouseout', _mouseOut)

      // mobile event listeners
      elt.on('touchstart', _mouseDown, false)
    })

    // add mousedown and mousehover handler for columns
    ylabels.each( (e,i) => {
      let elt = d3.select('.line.line-'+(i+1))
      let _mouseDown = (d,i2) => {
        let pos = d3.mouse(svg) // returns [x,y]
        automata = {
          state: DragState.Moving,
          pos:{y:pos[1],x:pos[0]},
          elts: d3.selectAll('.line-'+(i+1)),
          symelts: d3.selectAll('.colm-'+(i+1)),
          move: MoveType.Line,
          index: i
        }
      }
      elt.on('mousedown', _mouseDown)
      elt.on('mouseenter', _mouseEnter)
      elt.on('mouseout', _mouseOut)

      // mobile event listeners
      elt.on('touchstart', _mouseDown, false)
    })

    // add mousemove handler for automata
    let _mouseMove = (d,i) => {
        if ( automata.state === DragState.Moving ) {
          // compute new position according to mouse
          let pos = d3.mouse(svg) // returns [x,y]
          if ( automata.move == MoveType.Line ) {
            automata.elts.attr('transform', `translate(${0},${-automata.pos.y+pos[1]})`)
            if ( self.instance.symmetric ) {
                automata.symelts.attr('transform', `translate(${-automata.pos.y+pos[1]}, ${0})`)
            }
          } else {
            automata.elts.attr('transform', `translate(${-automata.pos.x+pos[0]},${0})`)
            if ( self.instance.symmetric ) {
                automata.symelts.attr('transform', `translate(${0}, ${-automata.pos.x+pos[0]})`)
            }
          }
          moveElts(self, {offsetX: pos[0], offsetY: pos[1]}, automata)
        //   if ( self.instance.symmetric ) {
        //     moveElts(self, {offsetY: pos[0], offsetX: pos[1]}, automata, true)
        //   }
        }
      }
      svgd3.on('mousemove', _mouseMove)
  
      // add mouseup handler for automata
      let _mouseUp = (d,i) => {
        if ( automata.state === DragState.Moving ) {
          let pos = d3.mouse(svg) // returns [x,y]
          automata.state = DragState.Idle
          automata.elts = null
          fixPositions(self, automata, {offsetX: pos[0], offsetY: pos[1]})
          if ( self.instance.symmetric ) {
            fixPositions(self, automata, {offsetY: pos[0], offsetX: pos[1]}, true)
          }
        }
      }
      svgd3.on('mouseup', _mouseUp)
  
      svgd3.on('touchend', _mouseUp, false)
      svgd3.on('touchmove', _mouseMove, false)
}

function moveElts(self, ev:{offsetX:number, offsetY:number}, automata, symmetric_call = false) {
  let d = self.instance
  let move_line = (automata.move === MoveType.Line && !symmetric_call) || (automata.move !== MoveType.Line && symmetric_call)
  if ( move_line ) {
    let i = findIndex(self, ev, MoveType.Line)
    let tileH = d.y_height[d.props.y[automata.index]]
    // modify position of each node after index
    for ( let j = 0 ; j < d.props.y.length ; j++ ) {
      let elts = Array.prototype.slice.call(document.querySelectorAll('.line-'+(j+1)))
      elts.forEach( (elt) => {
        if ( j > automata.index ) {
          if ( j > i ) {
            elt.setAttribute('transform', `translate(0,0)`)
          } else {
            elt.setAttribute('transform', `translate(0,${-(tileH+1)})`)
          }
        }
        else if ( j < automata.index ) {
          if ( j >= i ) {
            elt.setAttribute('transform', `translate(0,${(tileH+1)})`)
          } else {
            elt.setAttribute('transform', `translate(0,0)`)
          }
        }
      })
    }
  } else { // move column
    let i = findIndex(self, ev, MoveType.Column)
    let tileW = d.x_width[d.props.x[automata.index]]    
    // modify position of each node after index
    for ( let j = 0 ; j < d.props.x.length ; j++ ) {
      let elts = Array.prototype.slice.call(document.querySelectorAll('.colm-'+(j+1)))
      elts.forEach( (elt) => {
        if ( j > automata.index ) {
          if ( j > i ) {
            elt.setAttribute('transform', `translate(0,0)`)
          } else {
            elt.setAttribute('transform', `translate(${-(tileW+1)},0)`)
          }
        }
        else if ( j < automata.index ) {
          if ( j >= i ) {
            elt.setAttribute('transform', `translate(${(tileW+1)},0)`)
          } else {
            elt.setAttribute('transform', `translate(0,0)`)
          }
        }
      })
    }
  }
}

function fixPositions(self, automata, ev:{offsetX: number, offsetY: number}, symmetric_call=false) {
    let move_line = (automata.move === MoveType.Line && !symmetric_call) || (automata.move !== MoveType.Line && symmetric_call)
    if ( move_line ) {
        let i = findIndex(self, ev, move_line?MoveType.Line:MoveType.Column)
        changePos(self, automata.index, i, move_line?MoveType.Line:MoveType.Column)
      } else {
        let i = findIndex(self, ev, move_line?MoveType.Line:MoveType.Column)
        changePos(self, automata.index, i, move_line?MoveType.Line:MoveType.Column)
      }
      // replace all elements
      let elts = Array.prototype.slice.call(document.querySelectorAll('.elt'))
      elts.forEach( (elt) => {
        elt.setAttribute('transform', `translate(0,0)`)
      })
}

function findIndex(self, ev:{offsetX:number,offsetY:number}, m:MoveType): number {
    let tab, obj, acc
    if ( m === MoveType.Line ) {
        tab = self.instance.props.y.map( e => {return self.instance.y_height[e]})
        obj = ev.offsetY
        acc = self.instance.y_title_width
    } else {
        tab = self.instance.props.x.map( e => {return self.instance.x_width[e]})
        obj = ev.offsetX
        acc = self.instance.x_title_height
    }
    for ( let i = 0 ; i < tab.length ; i++ ) {
        if ( obj < acc + tab[i] ) {
            return i
        }
        acc += tab[i]
    }
    return tab.length-1
}

function modifyInstance(tab, from, to) {
  let res = []
  let b = 0
  for ( let a = 0 ; a < tab.length ; a++ ) {
    if ( b == from ) {
      b++
    }
    if ( a !== to ) {
      res.push(tab[b])
      b++
    } else {
      res.push(tab[from])
    }
  }
  return res
}

function changePos(self, from:number, to:number, move:MoveType) {
    let obj
    if ( move === MoveType.Line ) {
      obj = {
        y: modifyInstance(self.instance.props.y, from, to),
        x: self.instance.props.x,
        r: self.instance.props.r
      }
    } else {
      obj = {
        x: modifyInstance(self.instance.props.x, from, to),
        y: self.instance.props.y,
        r: self.instance.props.r
      }
    }
    Vue.set(self.instance, 'props', obj)
    self.$emit('update', JSON.stringify(self.instance))
}

export function addClickHandler(self) {
    let tiles = d3.selectAll('.tile')
    tiles.on('click', function(d:any,i) {
        let x = self.instance.props.x[d.idx-1]
        let y = self.instance.props.y[d.idy-1]
        self.$emit('click', {y:y,x:x})
    })


}