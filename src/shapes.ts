import * as d3 from "d3"
import { initDrag, addClickHandler } from "./interactions"

function initSvg(self): void {
    self.$forceUpdate()
    let svg = document.querySelector('svg')
    // remove all elts from svg
    while (svg.firstChild) {
      svg.removeChild(svg.firstChild);
    }

    // add lines and columns identifiers
    let svgd3 = d3.select('svg')
        .style("border", "1px solid #666")
        .style("touch-action", "none")

    let xlabels = svgd3.selectAll(".colm")
      .data(self.getXLabels())
      .enter()
      .append("g")

    xlabels.append('rect')
        .attr('x', (t:any) => { return t.x })
        .attr('y', (t:any) => { return t.y })
        .attr('width', (t:any) => { return t.w })
        .attr('height', (t:any) => { return t.h })
        .attr('fill', (t:any) => { return t.color })
        .attr('class', (t:any) => { return "elt colm colm-"+t.idx} )

    xlabels.append('text')
        .attr('x', (t:any) => { return t.x+t.w/2 })
        .attr('y', (t:any) => { return t.y+t.h*2/3 })
        .text( (t:any) => { return t.text} )
        .attr('font-family', 'sans-serif')
        .attr('font-size', (t:any) => { return `${Math.floor(Math.min(t.w,t.h)*0.5)}px` } )
        .attr('fill', self.instance.x_title_textcolor)
        .attr('alignment-baseline', 'middle')
        .attr('text-anchor', 'middle')
        .attr('class', (t:any) => { return `xtext elt colm-${t.idx}` })
        .style('-moz-user-select', '-moz-none')
        .style('user-select', 'none')
        .style('pointer-events', 'none')
    
    let ylabels = svgd3.selectAll(".line")
        .data(self.getYLabels())
        .enter()
        .append('g')

    ylabels.append("rect")
            .attr('x', (t:any) => { return t.x })
            .attr('y', (t:any) => { return t.y })
            .attr('width', (t:any) => { return t.w })
            .attr('height', (t:any) => { return t.h })
            .attr('fill', (t:any) => { return t.color })
            .attr('class', (t:any) => { return "elt line line-"+t.idy} )

    ylabels.append('text')
    .attr('x', (t:any) => { return t.x+t.w/2 })
    .attr('y', (t:any) => { return t.y+t.h/2 })
    .text( (t:any) => { return t.text} )
    .attr('font-family', 'sans-serif')
    .attr('font-size', (t:any) => { return `${Math.floor(Math.min(t.w,t.h)*0.5)}px` } )
    .attr('fill', self.instance.y_title_textcolor)
    .attr('alignment-baseline', 'middle')
    .attr('text-anchor', 'middle')
    .attr('class', (t:any) => { return `ytext elt line-${t.idy}` })
    .style('-moz-user-select', '-moz-none')
    .style('user-select', 'none')
    .style('pointer-events', 'none')


    // add tiles
    let tiles = svgd3.selectAll(".tile")
        .data(self.getTiles())
        .enter()
        .append('g')
    tiles.append("rect")
        .attr('x', (t:any) => { return t.x })
        .attr('y', (t:any) => { return t.y })
        .attr('width', (t:any) => { return t.w })
        .attr('height', (t:any) => { return t.h })
        .attr('fill', (t:any) => { return t.color })
        .attr('class', (t:any) => { return `elt tile colm-${t.idx} line-${t.idy}` } )
    tiles.append("text")
        .attr('x', (t:any) => { return t.x+t.w/2 })
        .attr('y', (t:any) => { return t.y+t.h/3*2 })
        .text( (t:any) => { return t.text.ok ? t.text.text : "" })
        .attr('font-family', 'sans-serif')
        .attr('font-size', (t:any) => { return `${Math.floor(Math.min(t.w,t.h)*0.5)}px` } )
        .attr('fill', (t:any) => { return t.text.ok ? t.text.color : "#fff"})
        .attr('alignment-baseline', 'middle')
        .attr('text-anchor', 'middle')
        .style('-moz-user-select', '-moz-none')
        .style('user-select', 'none')
        .style('pointer-events', 'none')
}


export function initInterface(self): void {
    initSvg(self)
    initDrag(self)
    addClickHandler(self)
}