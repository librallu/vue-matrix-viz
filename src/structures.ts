export interface Instance {
    props: {
        y: string[],
        x: string[],
        r: { [propname: string]: string[] },
    },
    x_width: { [propname: string]: number},
    x_title_height: number,
    y_height: { [propname: string]: number},
    y_title_width: number,
    separator: number,
    editable: boolean,
    randomize: boolean,
    symmetric: boolean
}

export interface Tile {
  x: number,
  y: number,
  color: string,
  text?: string,
  textColor?: string
}


export enum DragState {
  Idle,
  Moving
}

export interface Coord {
  x: number,
  y: number
}

export enum MoveType {
  Line,
  Column
}

export interface DragAutomata {
  state: DragState,
  pos: Coord,
  elts: Selection,
  move: MoveType,
  index: number
}