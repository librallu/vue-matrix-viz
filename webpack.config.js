var path = require('path')
var webpack = require('webpack')
var HtmlWebpackPlugin = require("html-webpack-plugin")
var glob = require('glob')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var merge = require("webpack-merge");
const CompressionPlugin = require("compression-webpack-plugin")


let commonConfig = {
  entry: './src/main.ts',
  output: {
    path: path.resolve(__dirname, './build/'),
    filename: 'build.js'
  },
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        exclude: /node_modules/,
        options: {
          appendTsSuffixTo: [/\.vue$/],
        }
      },
      {
        test: /\.(png|jpe?g|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name][hash].[ext]'
        }
      },
      {
        test: /\.(ttf|eot|woff|woff2)$/,
        loader: "file-loader",
      }
    ]
  },
  resolve: {
    extensions: ['.ts', '.js', '.vue', '.json'],
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: 'Template App',
      template: 'src/index.html'
    })
  ]
}



let devConfig = {
  module:{
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': 'vue-style-loader!css-loader!sass-loader'
          }
        }
      },
            {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test: /\.scss$/,
        use: [
          { loader: "style-loader" },
          { loader: "css-loader", options: { sourceMap: true } },
          { loader: "sass-loader", options: { sourceMap: true } }
        ]
      }
    ]
  },
  watchOptions: {
    poll: true
  },
  devServer: {
    inline: true
  },
  devtool: '#eval-source-map'
}



let prodConfig = {
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            'scss': ExtractTextPlugin.extract({
              fallback: "vue-style-loader", use:["css-loader", "sass-loader"]
            }),
            'css': ExtractTextPlugin.extract({
              fallback: "vue-style-loader", use:["css-loader"]
            }),
          }
        }
      },
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use:["css-loader"]})
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({fallback: "style-loader", use:["css-loader", "sass-loader"]})
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin("styles.css"),
    new CompressionPlugin({
      test: /\.js/
    })
  ]
}

let libConfig = {
  entry: path.resolve(__dirname + '/src/wrapper.ts'),
  output: {
    path: path.resolve(__dirname, './build'),
    publicPath: '/build/',
    filename: 'vue-matrix-viz.min.js',
    libraryTarget: 'umd',
    library: 'vue-matrix-viz',
    umdNamedDefine: true
  }
}


module.exports = mode => {
  if ( mode === "production" ) {
    return [
      merge(commonConfig, prodConfig),
      merge(merge(commonConfig, prodConfig), libConfig)
    ]
  } else {
    return merge(commonConfig, devConfig)
  }
}
